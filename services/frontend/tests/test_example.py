import pytest


def get_special_int():
    return 14


@pytest.fixture
def very_special_int():
    return 14


def test_special_int(very_special_int):
    assert very_special_int == get_special_int()
    assert get_special_int() != 5
    assert get_special_int() == 14
