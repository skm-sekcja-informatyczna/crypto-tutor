from flask import Flask, Blueprint, render_template, request
import grpc
import routes_pb2 as routes_pb2
import routes_pb2_grpc as routes_pb2_grpc


def create_app():
    app = Flask(__name__)

    home = Blueprint("home", __name__)
    htmx_api = Blueprint("api", __name__)

    @home.route("/")
    def main():
        return render_template("index.html")

    @htmx_api.route("/encrypt", methods=["POST"])
    def encrypt():
        with grpc.insecure_channel("localhost:50051") as channel:
            stub = routes_pb2_grpc.CryptoEngineStub(channel)
            response = stub.Encrypt(
                routes_pb2.CryptoRequest(
                    msg=request.form["message"],
                    encoder=routes_pb2.EncoderInfo(
                        encoder_type=routes_pb2.EncoderType.ENCODER_SIMPLE_ALPHABETIC
                    ),
                    cipher=routes_pb2.CipherInfo(
                        cipher_type=routes_pb2.CipherType.CIPHER_CESAR,
                        key=2,
                        alphabet_size=26,
                    ),
                )
            )
            print("Greeter client received: " + response.msg)
            return response.msg

    @htmx_api.route("/fields")
    def get_fields():
        return "foo"

    app.register_blueprint(home, url_prefix="/")
    app.register_blueprint(htmx_api, url_prefix="/api")

    return app
