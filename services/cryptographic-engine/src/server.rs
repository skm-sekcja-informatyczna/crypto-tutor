use tonic::{transport::Server, Request, Response, Status};

use crypto_engine::crypto_engine_server::{CryptoEngine, CryptoEngineServer};
use crypto_engine::{CipherInfo, CipherType, CryptoReply, CryptoRequest, EncoderInfo, EncoderType};

use crypto_systems::ciphers::CesarCipher;
use crypto_systems::crypto_pipeline::{CryptoPipeline, TCryptoPipeline};
use crypto_systems::encoders::SimpleAlphabeticEncoder;

pub mod crypto_engine {
    tonic::include_proto!("cryptoengine"); // The string specified here must match the proto package name
}

enum PipelineCreationError {
    EncoderNotSpecified,
    EncoderIncorectArgs,
    CipherNotSpecified,
    CipherIncorrectArgs,
    EncoderIncompatibleWithCipher,
}

impl PipelineCreationError {
    fn to_status(&self) -> Status {
        match self {
            Self::EncoderNotSpecified => Status::invalid_argument("Encoder type not specified"),
            Self::EncoderIncorectArgs => Status::invalid_argument("Encoder arguments are invalid"),
            Self::CipherNotSpecified => Status::invalid_argument("Cipher type not specified"),
            Self::CipherIncorrectArgs => Status::invalid_argument("Cipher arguments are invalid"),
            Self::EncoderIncompatibleWithCipher => {
                Status::invalid_argument("Requested encoder and cipher are incompatible")
            }
        }
    }
}

fn create_pipeline(
    encoder_info: &EncoderInfo,
    cipher_info: &CipherInfo,
) -> Result<Box<dyn TCryptoPipeline>, PipelineCreationError> {
    if encoder_info.encoder_type() == EncoderType::EncoderUnspecified {
        return Err(PipelineCreationError::EncoderNotSpecified);
    }

    if cipher_info.cipher_type() == CipherType::CipherUnspecified {
        return Err(PipelineCreationError::CipherNotSpecified);
    }
    return Ok(Box::new(CryptoPipeline::new(
        Box::new(SimpleAlphabeticEncoder {}),
        Box::new(CesarCipher {
            key: cipher_info.key,
            modulus: 26,
        }),
    )));
}

#[derive(Debug, Default)]
pub struct MyCryptoEngine {}

#[tonic::async_trait]
impl CryptoEngine for MyCryptoEngine {
    async fn encrypt(
        &self,
        request: Request<CryptoRequest>,
    ) -> Result<Response<CryptoReply>, Status> {
        println!("Got a encrypt request: {:?}", request);

        let request_data = request.into_inner();
        let message = request_data.msg;
        let cipher_info = request_data.cipher.unwrap();
        let encoder_info = request_data.encoder.unwrap();

        let pipeline =
            create_pipeline(&encoder_info, &cipher_info).map_err(|err| err.to_status())?;

        let reply = crypto_engine::CryptoReply {
            msg: pipeline.encrypt(&message),
        };

        Ok(Response::new(reply))
    }
    async fn decrypt(
        &self,
        request: Request<CryptoRequest>, // Accept request of type HelloRequest
    ) -> Result<Response<CryptoReply>, Status> {
        // Return an instance of type HelloReply
        println!("Got decrypt a request: {:?}", request);
        let request_data = request.into_inner();
        let message = request_data.msg;
        let cipher_info = request_data.cipher.unwrap();
        let encoder_info = request_data.encoder.unwrap();

        let pipeline =
            create_pipeline(&encoder_info, &cipher_info).map_err(|err| err.to_status())?;

        let reply = crypto_engine::CryptoReply {
            msg: pipeline.decrypt(&message),
        };

        Ok(Response::new(reply))
    }
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let addr = "[::1]:50051".parse()?;
    let greeter = MyCryptoEngine::default();

    println!("Server will run on port 50051");

    Server::builder()
        .add_service(CryptoEngineServer::new(greeter))
        .serve(addr)
        .await?;

    Ok(())
}
