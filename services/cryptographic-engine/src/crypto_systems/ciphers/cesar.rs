use super::Cipher;

pub struct CesarCipher {
    pub key: i32,
    pub modulus: u32,
}

impl CesarCipher {
    fn encrypt_character(&self, character: &u32) -> u32 {
        return (*character as i32 + self.key)
            .rem_euclid(self.modulus as i32)
            .try_into()
            .unwrap();
    }
    fn decrypt_character(&self, character: &u32) -> u32 {
        return (*character as i32 - self.key)
            .rem_euclid(self.modulus as i32)
            .try_into()
            .unwrap();
    }
}

impl Cipher for CesarCipher {
    type CipherChar = u32;

    fn encrypt(&self, message: &Vec<Self::CipherChar>) -> Vec<Self::CipherChar> {
        return message
            .into_iter()
            .map(|x| self.encrypt_character(&x))
            .collect();
    }
    fn decrypt(&self, message: &Vec<Self::CipherChar>) -> Vec<Self::CipherChar> {
        return message
            .into_iter()
            .map(|x| self.decrypt_character(&x))
            .collect();
    }
}

#[cfg(test)]
mod tests {
    use super::CesarCipher;
    use super::Cipher;

    #[test]
    fn encrypt() {
        let cesar = CesarCipher {
            key: 7,
            modulus: 25,
        };
        let plain1: Vec<u32> = vec![1, 25, 12, 2, 8];
        let plain2: Vec<u32> = vec![0, 100, 22, 15, 23];

        assert_eq!(cesar.encrypt(&plain1), vec!(8, 7, 19, 9, 15));
        assert_eq!(cesar.encrypt(&plain2), vec!(7, 7, 4, 22, 5));
    }
    #[test]
    fn decrypt() {
        let cesar = CesarCipher {
            key: 7,
            modulus: 25,
        };
        let plain1: Vec<u32> = vec![1, 0, 12, 2, 8];
        let plain2: Vec<u32> = vec![0, 0, 22, 15, 23];

        assert_eq!(cesar.decrypt(&vec!(8, 7, 19, 9, 15)), plain1);
        assert_eq!(cesar.decrypt(&vec!(7, 7, 4, 22, 5)), plain2);
    }
}
