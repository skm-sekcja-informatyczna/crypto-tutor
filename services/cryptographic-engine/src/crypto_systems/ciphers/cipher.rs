pub trait Cipher {
    type CipherChar;

    fn encrypt(&self, message: &Vec<Self::CipherChar>) -> Vec<Self::CipherChar>;
    fn decrypt(&self, message: &Vec<Self::CipherChar>) -> Vec<Self::CipherChar>;
}
