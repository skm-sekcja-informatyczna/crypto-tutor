use super::ciphers::Cipher;
use super::encoders::Encoder;

pub trait TCryptoPipeline {
    fn encrypt(&self, msg: &str) -> String;
    fn decrypt(&self, msg: &str) -> String;
}

pub struct CryptoPipeline<T> {
    encoder: Box<dyn Encoder<EncodedType = T>>,
    cipher: Box<dyn Cipher<CipherChar = T>>,
}

impl<T> CryptoPipeline<T> {
    pub fn new(
        encoder: Box<dyn Encoder<EncodedType = T>>,
        cipher: Box<dyn Cipher<CipherChar = T>>,
    ) -> Self {
        CryptoPipeline { encoder, cipher }
    }
}

impl<T> TCryptoPipeline for CryptoPipeline<T> {
    fn encrypt(&self, message: &str) -> String {
        let mut temp = self.encoder.encode(message);
        temp = self.cipher.encrypt(&temp);
        return self.encoder.decode(&temp);
    }
    fn decrypt(&self, message: &str) -> String {
        let mut temp = self.encoder.encode(message);
        temp = self.cipher.decrypt(&temp);
        return self.encoder.decode(&temp);
    }
}
