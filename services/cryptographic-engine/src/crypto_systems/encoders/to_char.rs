pub struct ToCharEncoder;
use super::Encoder;


impl Encoder for ToCharEncoder {
    type EncodedType = char;

    fn encode(&self,message: &str) -> Vec<Self::EncodedType> {
        message.chars().collect()
    }

    fn decode(&self,message: &Vec<Self::EncodedType>) -> String {
        message.into_iter().collect()
    }
}


#[cfg(test)]
mod tests {
    use super::Encoder;
    use super::ToCharEncoder;


    #[test]
    fn encode() {
        let encoder = ToCharEncoder {};

        let mess1 = "asdasd";
        let excepted1: Vec<char> = vec!('a','s','d','a','s','d');
        assert_eq!(encoder.encode(mess1), excepted1);

        let mess2 = "DSACV";
        let excepted2: Vec<char> = vec!('D','S','A','C','V');
        assert_eq!(encoder.encode(mess2), excepted2);

        let mess3 = "123123";
        let excepted3: Vec<char> = vec!('1','2','3','1','2','3');
        assert_eq!(encoder.encode(mess3), excepted3);

        let mess4 = "      ";
        let excepted4: Vec<char> = vec!(' ',' ',' ',' ',' ',' ');
        assert_eq!(encoder.encode(mess4), excepted4);
    }


    #[test]
    fn empty_string_test() {
        let encoder = ToCharEncoder {};

        let message = "";
        let excepted: Vec<char> = vec!();
        assert_eq!(encoder.encode(message),excepted);
    }

    #[test]
    fn special_char_test() {
        let encoder = ToCharEncoder {};

        let message = "!@#$%^&*(";
        let excepted: Vec<char> = vec!('!','@','#','$','%','^','&','*','(');
        assert_eq!(encoder.encode(message),excepted);
    }  
}
