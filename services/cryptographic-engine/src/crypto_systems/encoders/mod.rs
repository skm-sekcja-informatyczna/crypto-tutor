pub mod encoder;
pub use encoder::Encoder;
pub mod simple_alphabetic;
pub use simple_alphabetic::SimpleAlphabeticEncoder;
pub mod to_char;
pub use to_char::ToCharEncoder;

