pub trait Encoder {
    type EncodedType;

    fn encode(&self, message: &str) -> Vec<Self::EncodedType>;
    fn decode(&self, message: &Vec<Self::EncodedType>) -> String;
}
