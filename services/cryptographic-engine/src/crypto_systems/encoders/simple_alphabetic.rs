use super::Encoder;
pub struct SimpleAlphabeticEncoder {}

impl SimpleAlphabeticEncoder {
    fn encode_character(&self, character: &char) -> Option<u32> {
        if character.is_ascii_alphabetic() {
            let a_ascii_code = 'a' as u32;
            let character_ascii_code = character.to_ascii_lowercase() as u32;
            return Some(character_ascii_code - a_ascii_code);
        } else {
            return None;
        }
    }
    fn decode_character(&self, character: &u32) -> Option<char> {
        return match character {
            0..=25 => char::from_u32(*character + 'a' as u32),
            _ => None,
        };
    }
}

impl Encoder for SimpleAlphabeticEncoder {
    type EncodedType = u32;

    fn encode(&self, message: &str) -> Vec<Self::EncodedType> {
        return message
            .chars()
            .map(|x| self.encode_character(&x))
            .flatten()
            .collect();
    }
    fn decode(&self, message: &Vec<Self::EncodedType>) -> String {
        return message
            .into_iter()
            .map(|x| self.decode_character(&x))
            .flatten()
            .collect();
    }
}

#[cfg(test)]
mod tests {
    use super::Encoder;
    use super::SimpleAlphabeticEncoder;

    #[test]
    fn encrypt() {
        let encoder = SimpleAlphabeticEncoder {};
        let plain1 = "abcd   efghijklmnopqrstuvwxyzłą$";
        let plain2 = "↓óżź*(ABCDEFGHIJKLMNOPQRSTUV WXYZ";

        assert_eq!(encoder.encode(&plain1), (0..26).collect::<Vec<u32>>());
        assert_eq!(encoder.encode(&plain2), (0..26).collect::<Vec<u32>>());
    }
    #[test]
    fn decrypt() {
        let encoder = SimpleAlphabeticEncoder {};

        let expected = "abcdefghijklmnopqrstuvwxyz";

        assert_eq!(encoder.decode(&(0..26).collect()), expected);
    }
}
