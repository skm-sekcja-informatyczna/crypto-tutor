# crypto-tutor

## Description
This project is meant to provide examples of cryptographic systems examples to play with and explanations of how they work. System consists of:
* Cryptographic-engine - backend which will provide all cryptographic capabilities
* Frontend - flask app that will display GUI in which cryptographic tools can be tested

## Running locally

### cryptographic-engine

To run you need to have installed:
* Rust and cargo
* protoc

To run program just run:

`cargo run`

You can also compile program manually with 

`cargo build` 

then runnable executable should be available in `path/to/repo/services/cryptographic-engine/target/debug/cryptographic-engine`

**Note**: Both commands will try to use protoc to build API definition from proto files.

### frontend

Requirements:

* Python 3.10 or higher
* protoc

Preferably do this steps using python virtualenv, see more here: https://docs.python.org/3/library/venv.html

**Note**: If you have problems with creating/activating virtualenvironment on Windows try creating it with `py` instead of `python`:

`py -m venv venv`

We will show assuming that we are inside repository path: `path/to/repo/services/frontend` with virtual environment in directory `./venv`

1. First activate your virtual environment
* Linux: `source venv/bin/activate/`
* Windows `.\venv\Scripts\activate`

2. Next if not already installed please install requirements:

`pip install -r requirements-base.txt`
 
On windows you may need to use:

`py -m pip install -r requirements-base.txt`

3. Then change your directory to src with `cd src`. Now you can compile API contracts using:

`python -m grpc_tools.protoc  -I=../../cryptographic-engine/proto --python_out=./proto/ --grpc_python_out=./proto/ ../../cryptographic-engine/proto/routes.proto`

On Windows you may need to instead use:

`py -m grpc_tools.protoc  -I=../../cryptographic-engine/proto --python_out=./proto/ --grpc_python_out=./proto/ ../../cryptographic-engine/proto/routes.proto`

4. Now you are ready to run app using:

`flask run`

As before on Windows machines you may need to use:

`py -m flask run`

**Note:** For app to work properly cryptographic engine must be available on port 50051

## Running tests
### cryptographic-engine
To run tests please run

`cargo test`

### frontend
To run tests again it is recommended to use virtual environment, acrtivate as shown above in "Running locally" section. 
We will assume that you are already with your venv activated and in `path/to/repo/services/frontend` directory. 

1. For testing install test requirements:

`pip install -r requirements-tests.txt`

Or on Windows:

`py -m pip install -r requirements.txt`

2. Then run tests with pytest:

`pytest`

On windows you may need to use:

`py -m pytest`

3. You can also run specific files/directories like this:

`pytest tests/test_i_wanna_run.py`

Or:

`py -m pytest tests/test_i_wanna_run.py`


## Deployment/Installation

